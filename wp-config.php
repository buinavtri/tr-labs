<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// Define URL
define('WP_HOME','http://trlab.cf');
define('WP_SITEURL','http://trlab.cf');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'trl-db' );

/** MySQL database username */
define( 'DB_USER', 'trl-user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'triz_pri_eBE_uFic3&1' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BGY|I0_vu`|Q>ZNp+pK{(BiR-+3a;-VY_;Z-*EF}BZWtaa+5RTdl6|1]85hBzgR`');
define('SECURE_AUTH_KEY',  'jr1kH&cUf/y7+|/60&|a1S3&gJT/S-`S.(u>FVVdbd#j::5+E`&?X#P,e#++Sg02');
define('LOGGED_IN_KEY',    '(`i#2;z0q1Gq+Ev^[QS.XnvIw|:-v= P6}qv[cCyp]3}H@L;WW,WQ;[E1JviWVy(');
define('NONCE_KEY',        'K+FdEOH? |d{YG2{HQ&u;`:vHEV`R,|SIYMeBC@]40k}sqN*4$A9eq@a^Wn$E3d;');
define('AUTH_SALT',        'uy`+VVNRX8T|A7E_lF-tKDz=o0pH4iku`Cit!e)#mP<V#3iW9E/qf_3JgWymXt=m');
define('SECURE_AUTH_SALT', 'S|h&tzC2}]}L![bazCj dB-=#fi&%OrGBhP;D)oW/%A%qrHv]usu83mGXoIlRCCh');
define('LOGGED_IN_SALT',   '&-!>k&B:-G~J)L<7-K3/ZW}+y#?eKHZCx>rWO/5{Y4iiJIzu;]}YuK:8EkPNA_v5');
define('NONCE_SALT',       '4oC_lv`,P8>]GY)NAqKj:GCTro) J8rgPi2tGhTebSPYd`[Q>~0Y;z)$E+6T7e`&');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
